import Vue from 'vue';
import VueFormWizard from 'vue-form-wizard';
import 'vue-form-wizard/dist/vue-form-wizard.min.css';
import App from './App.vue';
import Home from './views/Home.vue';
import router from './router';
require("babel-polyfill");
require("datalist-polyfill");
Vue.use(VueFormWizard);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
msieversion();

function msieversion() {

  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
  {

  }
  else  // If another browser, return 0
  {
    new Vue({
      router,
      //render: h => h(App), // with router
      render: h => h(Home),
      data: {
        API_URL: 'API'
      }
    }).$mount('#app');
  }

  return false;
}

