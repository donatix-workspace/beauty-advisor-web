// vue.config.js
module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ?
    './static/' : '/',
  filenameHashing: true,
  devServer: {
    proxy: {
      'http://ebeauty.donatix.info': {
        target: 'http://ebeauty.donatix.info',
        ws: true,
        changeOrigin: true,
      },
    },
  },
}
